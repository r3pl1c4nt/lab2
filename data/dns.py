file_name = "hosts_new.txt"
with open(file_name, "r") as f:
    hosts_lines = f.readlines()
    hosts_lines = [line.rstrip() for line in hosts_lines]

file_name = "dns.log"
with open(file_name, "r") as f:
    dns_lines = f.readlines()

bad_reqs = 0

for line in dns_lines:
    if (not line.startswith("#")) and (line.split("\t")[9] in hosts_lines):
        bad_reqs += 1 

perc = (bad_reqs / (sum(not line.startswith("#") for line in dns_lines))) * 100
print("Количество хостов в дампе трафика =", len(dns_lines))
print ("Процент 'плохих' хостов =", perc)
